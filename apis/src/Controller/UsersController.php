<?php
namespace App\Controller;

use App\Controller\AppController;


class UsersController extends AppController {
    // public function view($id = null) {
    //   $user = $this->Users->get($id, [
    //     'contain' => [],
    //   ]);
    //   $this->set('user', $user);
    // }

    public function add() {
      $this->autoRender = false;
      if ($this->CheckRequest('post')) {
          $data = $this->request->getData();
          $lastDigits = $this->Users->find()->count() +1;
          $data['code'] = 'MB-'. $this->createCode().$lastDigits;
          $data['first_name'] = $this->capitalizeFirstLetter($data['first_name']);
          $data['middle_name'] = $this->capitalizeFirstLetter($data['middle_name']);
          $data['last_name'] = $this->capitalizeFirstLetter($data['last_name']);
          $user = $this->Users->newEntity();
          $user = $this->Users->patchEntity($user, $data);
          $duplicateUsername = $this->Users->find()->where(['username' => $data['username']])->count();
          $duplicateEmail = $this->Users->find()->where(['email' => $data['email']])->count();
          if ($duplicateUsername === 0 && $duplicateEmail === 0) {
              $name = $this->capitalizeFirstLetter($data['first_name']);
              $code = $data['code'];
              $email = $data['email'];
              if ($this->Users->save($user)) {
                  $this->promtMessage = array('status'=>'success','message'=>'Yehey! You are now registered!');
                  $this->sendValidationLink($code,$name,$email);
              } else {
                  $errorList = [];
                  $errors = $user->errors();
                  foreach ($errors as $key => $value) {
                    foreach ($value as $key2 => $value2) {
                      array_push($errorList,array($key => $value2));
                    }
                  }
                  $this->promtMessage = array('status'=>'failed', 'message'=> $errorList);
              }
          } else {
              if ($duplicateUsername > 0 ) {
                  // response if email/username is existing
                  $this->promtMessage = array('status'=>'failed','message'=>'Username already taken');
                  $errorList = [];
                  array_push($errorList,array('username'=>'Username already taken'));
                  $this->promtMessage = array('status'=>'failed','message'=>$errorList);
              }
              if ($duplicateEmail > 0 ) { 
                  $errorList = [];
                  array_push($errorList,array('email'=>'Email already taken'));
                  $this->promtMessage = array('status'=>'failed','message'=>$errorList);
              }
          }
      }
      return $this->response
      ->withHeader('token', 'My header')
      ->withType('application/json')
      ->withStringBody(json_encode($this->promtMessage));
    }
    public function activate () {
      $this->autoRender = false;
      if ($this->CheckRequest('post')) {
          $data = $this->request->getData();
          $data['code'] = $this->cleanString($data['code']);
          $query = $this->Users->find('all')
          ->where(['BINARY code = '.'\''.$data['code'].'\'']);
          $record = $query->first();
          if (empty($record)) {
              $this->promtMessage = array('status'=>'failed','message'=>'Whoops, you entered an invalid code');
          } else {
              unset($record['modified']);
              if (!$record['activation_status']) {
                  $record['activation_status'] = 1;
                  if ($this->Users->save($record)) {
                      $this->promtMessage = array('status'=>'success','message'=>'Yehey! Your account was activated');
                  } else {
                      $this->promtMessage = array('status'=>'failed','message'=>$record->errors());
                  }
              } else {
                  $this->promtMessage = array('status'=>'success','message'=>'Your account was already activated');  
              }
          }
      }
      return $this->response
      ->withType('application/json')
      ->withStringBody(json_encode($this->promtMessage));
    }
    public function resendCode () {
      $this->autoRender = false;
      if ($this->CheckRequest('post')) {
          $data = $this->request->getData();
          $data['username'] = $this->cleanString( $data['username']);
          $query = $this->Users->find('all')
          ->where(['BINARY username = '=>$data['username']]);
          $record = $query->first();
          if (empty($record)) {
              $this->promtMessage = array('status'=>'failed','message'=>'Whoops, you entered an invalid username');
          } else {
              $resendEmail = $record['email'];
              $resendCode = $record['code'];
              $resendName =  $this->capitalizeFirstLetter($record['first_name']);
              $this->sendValidationLink($resendCode,$resendName,$resendEmail);
              $this->promtMessage = array('status'=>'success','message'=>'Yehey! Validation code resent! Check you email.');
          }
      }
      return $this->response
      ->withType('application/json')
      ->withStringBody(json_encode($this->promtMessage));
    }
    public function login () {
      $this->autoRender = false;
      if ($this->CheckRequest('post')) {
          $data = $this->request->getData();
          $data['username'] = $this->cleanString($data['username']);
          $data['password'] = $this->cleanString($data['password']);
          $query = $this->Users->find('all')
          ->where(['BINARY username = '=>$data['username']]);
          $record = $query->first();
          if (empty($record)) {
              $this->promtMessage = array('status'=>'failed', 'message'=>'Whoops, login failed');
          } else {
              if ($record['activation_status']) {
                  if ($this->checkPassword($data['password'],$record['password'])) {
                      $token = $this->createToken($data['username']);
                      $session = $this->request->session();
                      $session->write('User.token',$token );
                      $session->write('User.id',$record['id']);
                      $this->promtMessage = array('status'=>'success', 'message'=>'Login success, Welcome!','token'=>$token);
                  } else {
                      $this->promtMessage = array('status'=>'failed', 'message'=>'Whoops, login failed');
                  }
              } else {
                  $this->promtMessage = array('status'=>'failed', 'message'=>'The account you are using is not activated yet');
              }
          }
      }
      return $this->response
      ->withType('application/json')
      ->withStringBody(json_encode($this->promtMessage));
    }
    public function authenticate () {
      $this->autoRender = false;
      $data = $this->request->getData();
      if ($this->CheckRequest('post')) {
          $this->promtMessage = array('status'=>'failed', 'message'=>'Please complete the fields');
          $session = $this->request->session();
          if ($session->check('User.token')) {
              $baseToken = $session->read('User.token');
              if ($data['token'] === $baseToken) {
                  $this->promtMessage = array('status'=>'success', 'message'=>'Login success, Welcome!');
              }
          }
      }
      return $this->response
      ->withType('application/json')
      ->withStringBody(json_encode($this->promtMessage));
    }
    public  function logout () {
      $this->autoRender = false;
      if ($this->CheckRequest('post')) {
          $session = $this->request->session();
          $session->destroy();
      }
    }
    public  function getProfile () {
      $this->autoRender = false;
      if ($this->CheckRequest('post')) {
          if ($this->CheckSession('User.token')) {
              $data = $this->request->getData();
              $session = $this->request->session();
              $baseToken = $session->read('User.token');
              if ($data['token'] === $baseToken) {
                  $id = $session->read('User.id');
                  $query = $this->Users->find('all')
                    ->where(['id = '=> $id]);
                  $record = $query->first();
                  if (empty($record)) {
                      $this->promtMessage = array('status'=>'failed', 'message'=>'Please relogin');
                  } else {
                      $record['date_of_birth'] = date("M d, Y", strtotime($record['date_of_birth']));
                      $record['first_name'] = $this->capitalizeFirstLetter($record['first_name']);
                      $record['last_name'] = $this->capitalizeFirstLetter($record['last_name']);
                      $record['id2'] = $record['id'];
                      $record['id'] = $this->idEncryption($record['id']);
                      $this->promtMessage = array('status'=>'success', 'record'=>$record);
                  }
              }
          }
      }
      return $this->response
      ->withType('application/json')
      ->withStringBody(json_encode($this->promtMessage));
    }
    public function profilePic () {
      $this->autoRender = false;
      if ($this->CheckRequest('post')) {
          if ($this->CheckSession('User.token')) {
              $data = $this->request->getData();
              $session = $this->request->session();
              $baseToken = $session->read('User.token');
              if ($data['token'] === $baseToken) {
                  if (empty($_FILES['file'])) {
                      $this->promtMessage = array('status'=>'failed', 'message'=>'No photo was sent');
                  } else {
                      $userId = $session->read('User.id');
                      $img = $_FILES['file']['name'][0];
                      $tmp = $_FILES['file']['tmp_name'][0];
                      $path = '../../pic-profiles/';
                      $extension = pathinfo($img, PATHINFO_EXTENSION);
                      $imgNewName = strtolower($userId.$this->createCode()."-profilepic.".$extension);
                      $path = $path. $imgNewName;
                      $record = $this->Users->get($userId);
                      $record['image'] = $imgNewName;
                      if ($_FILES['file']['size'][0] <= 2000000) {
                          if ($this->Users->save($record)) {
                              if (move_uploaded_file($tmp,$path)) {
                                  $this->promtMessage = array('status'=>'success', 'message'=>"Image uploaded to server and database");
                              }
                          }
                      } else {
                          $this->promtMessage = array('status'=>'failed', 'message'=>'Image should not be more than 2mb in size');
                      }
                  }
              } else {
                  $this->promtMessage = array('status'=>'failed', 'message'=>'unauthorized');
              }
          }
      }
      return $this->response
      ->withType('application/json')
      ->withStringBody(json_encode($this->promtMessage));
    }
    public function editProfile () {
      $this->autoRender = false;
      if ($this->CheckRequest('post')) {
          if ($this->CheckSession('User.token')) {
              $data = $this->request->getData();
              $session = $this->request->session();
              $baseToken = $session->read('User.token');
              $baseId = $session->read('User.id');
              $data['id'] = $this->idDecryption($data['id']);
              if ($data['token'] === $baseToken && $baseId === $data['id']) {
                  $record = $this->Users->get($baseId);
                  if ($this->checkPassword($data['old_password'],$record['password'])) {
                      $record = $this->Users->patchEntity($record,$data);
                    if ($this->Users->save($record)) {
                        $this->promtMessage = array('status'=>'success', 'message'=>'Profile successfuilly updated!');
                    } else {
                        $errorList = [];
                        $errors = $record->errors();
                        foreach ($errors as $key => $value) {
                          foreach ($value as $key2 => $value2) {
                            array_push($errorList,array($key => $value2));
                          }
                        }
                        $this->promtMessage = array('status'=>'failed', 'message'=> $errorList);
                    }
                  } else {
                      $this->promtMessage = array('status'=>'failed', 'message'=>'Wrong old password');
                  }
              } else {
                  $this->promtMessage = array('status'=>'failed', 'message'=>'unauthorized');
              }
          }
      }
      return $this->response
      ->withType('application/json')
      ->withStringBody(json_encode($this->promtMessage));
    }
    public  function showProfile () {
      $this->autoRender = false;
      $userId = $this->request->query('user_id');
      $userId = intval($this->cleanNumber($this->idDecryption($userId)));
      $searchId = $this->request->query('search_id');
      //$searchId = $this->cleanNumber($this->idDecryption($searchId));
      $token = $this->cleanString($this->request->query('token'));
      if ($this->CheckRequest('get')) {
          if ($this->CheckSession('User.token')) {
              $session = $this->request->session();
              $baseToken = $session->read('User.token');
              $baseId = $session->read('User.id');
              if ($token === $baseToken && $baseId === $userId) {
                  $query = $this->Users->find('all')
                  ->select(['id','first_name','middle_name','last_name','date_of_birth','username','image'])
                  ->where(['id' => $searchId]);
                  $record = $query->first();
                  if (!empty($record)) {
                      $record['date_of_birth'] = date("M d, Y", strtotime($record['date_of_birth']));
                      $record['first_name'] = $this->capitalizeFirstLetter($record['first_name']);
                      $record['last_name'] = $this->capitalizeFirstLetter($record['last_name']);
                      $this->promtMessage = array('status'=>'success', 'record'=>$record);
                     
                  }
              } else {
                  $this->promtMessage = array('status'=>'failed', 'message'=>'unauthorized');
             }
          }
      }
      return $this->response
      ->withType('application/json')
      ->withStringBody(json_encode($this->promtMessage));
    }
    // public function edit($id = null)
    // {
    //     $user = $this->Users->get($id, [
    //         'contain' => [],
    //     ]);
    //     if ($this->request->is(['patch', 'post', 'put'])) {
    //         $user = $this->Users->patchEntity($user, $this->request->getData());
    //         if ($this->Users->save($user)) {
    //             $this->Flash->success(__('The user has been saved.'));

    //             return $this->redirect(['action' => 'index']);
    //         }
    //         $this->Flash->error(__('The user could not be saved. Please, try again.'));
    //     }
    //     $this->set(compact('user'));
    // }

    // public function delete($id = null)
    // {
    //     $this->request->allowMethod(['post', 'delete']);
    //     $user = $this->Users->get($id);
    //     if ($this->Users->delete($user)) {
    //         $this->Flash->success(__('The user has been deleted.'));
    //     } else {
    //         $this->Flash->error(__('The user could not be deleted. Please, try again.'));
    //     }

    //     return $this->redirect(['action' => 'index']);
    // }
}

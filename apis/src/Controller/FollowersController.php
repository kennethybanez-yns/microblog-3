<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\ORM\TableRegistry;

/**
 * Followers Controller
 *
 * @property \App\Model\Table\FollowersTable $Followers
 *
 * @method \App\Model\Entity\Follower[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class FollowersController extends AppController {
  public function viewPeople () {
    $this->autoRender = false;
    $userId = $this->request->query('id');
    $userId = intval($this->cleanNumber($this->idDecryption($userId)));
    $token = $this->cleanString($this->request->query('token'));
    $page = $this->cleanNumber($this->request->query('page'));
    $size = $this->cleanNumber($this->request->query('size'));
    $followersFollowedByMe = [];
    $followingThatFollowedBack = [];
    if ($this->CheckRequest('get')) {
        if ($this->CheckSession('User.token')) {
            $session = $this->request->session();
            $baseToken = $session->read('User.token');
            $baseId = $session->read('User.id');
            if ($token === $baseToken && $baseId === $userId) {
                $offset = ($page - 1) * $size;
                $totalFollowers = $this->Followers->find()->where(['following_id' => $userId,'deleted' => 1])->count();
                $totalFollowings = $this->Followers->find()->where(['user_id' => $userId,'deleted' => 1])->count();
                $followers = $this->Followers->find('all')
                  ->contain(['MyFollower'])
                  ->select($this->Followers)
                  ->select(['MyFollower.id','MyFollower.first_name','MyFollower.last_name','MyFollower.image'])
                  ->where(['following_id' => $userId,'Followers.deleted' => 1])
                  ->order(['Followers.created' => 'ASC'])
                  ->limit($size)
                  ->offset($offset)->formatResults(function($results) {
                      return $results->map(function($row) {
                        $row['created'] = date("M j Y g:i:s A", strtotime($row['created']));
                        return $row;
                      });
                  })
                  ->toArray();
                $followings = $this->Followers->find('all')
                  ->contain(['MyFollowing'])
                  ->select($this->Followers)
                  ->select(['MyFollowing.id','MyFollowing.first_name','MyFollowing.last_name','MyFollowing.image'])
                  ->where(['user_id' => $userId,'Followers.deleted' => 1])
                  ->order(['Followers.created' => 'ASC'])
                  ->limit($size)
                  ->offset($offset)
                  ->formatResults(function($results) {
                      return $results->map(function($row) {
                        $row['created'] = date("M j Y g:i:s A", strtotime($row['created']));
                        return $row;
                      });
                  })
                  ->toArray();
                  //echo json_encode($followers);
                  if (!empty($followers) || !empty($followings)) {
                    for ($followingCount=0; $followingCount < sizeof($followings); $followingCount++) {
                      for ($followerCount=0; $followerCount < sizeof($followers); $followerCount++) {
                        if ($followers[$followerCount]['my_follower']['id'] === $followings[$followingCount]['my_following']['id']) {
                            array_push($followersFollowedByMe,$followerCount);
                            array_push($followingThatFollowedBack,$followingCount);
                        }
                      }
                    }
                   // echo json_encode($followersFollowedByMe);
                    for ($i=0; $i < sizeof($followersFollowedByMe); $i++) {
                      $followers[$followersFollowedByMe[$i]]['my_follower']['followed'] = true;
                    }
                    for ($i=0; $i < sizeof($followingThatFollowedBack); $i++) {
                      $followings[$followingThatFollowedBack[$i]]['my_following']['followedBack'] = true;
                    }
                    for ($i=0; $i < sizeof($followers) ; $i++) {
                      $followers[$i]['id'] = $this->idEncryption($followers[$i]['id']);
                    }
                    for ($i=0; $i < sizeof($followings) ; $i++) {
                      $followings[$i]['id'] = $this->idEncryption($followings[$i]['id']);
                    }
                    $this->promtMessage = array('status'=>'success','totalFollowers' => $totalFollowers,'totalFollowings' => $totalFollowings, 'followerTotalPages'=>(ceil($totalFollowers/$size)),'followingTotalPages'=>(ceil($totalFollowings/$size)),'followers'=>$followers,'followings'=> $followings);
                  } else {
                    $followers = [];
                    $followings = [];
                    $this->promtMessage = array('status'=>'success', 'followers'=>$followers,'followings'=> $followings);
                  }
            } else {
                $this->promtMessage = array('status'=>'failed', 'message'=>'unauthorized');
            }
        }
    }
    return $this->response
    ->withHeader('token', 'My header')
    ->withType('application/json')
    ->withStringBody(json_encode($this->promtMessage));
  }
  public function follow () {
    $this->autoRender = false;
    if ($this->CheckRequest('post')) {
        if ($this->CheckSession('User.token')) {
            $data = $this->request->getData();
            $session = $this->request->session();
            $baseToken = $session->read('User.token');
            $baseId = $session->read('User.id');
            $data['user_id'] = $this->idDecryption($data['user_id']);
           // $data['following_id'] = $this->idDecryption($data['following_id']);
           
            if ($data['token'] === $baseToken && $baseId === $data['user_id']) {
                $duplicateCount = $this->Followers->find()->where(['user_id' => $data['user_id'],'following_id' => $data['following_id'],'Followers.deleted' => 1])->count();
                if ($duplicateCount < 1) {
                    $relation = $this->Followers->newEntity();
                    $relation = $this->Followers->patchEntity($relation, $data);
                    if ($this->Followers->save($relation)) {
                      $this->promtMessage = array('status'=>'success', 'message'=>'follow successful');
                  } else {
                      $errorList = [];
                      $errors = $relation->errors();
                      foreach ($errors as $key => $value) {
                        foreach ($value as $key2 => $value2) {
                          array_push($errorList,array($key => $value2));
                        }
                      }
                      $this->promtMessage = array('status'=>'failed', 'message'=> $errorList);
                  }
                }
            } else {
                $this->promtMessage = array('status'=>'failed', 'message'=>'unauthorized');
            }
        }
    }
    return $this->response
    ->withHeader('token', 'My header')
    ->withType('application/json')
    ->withStringBody(json_encode($this->promtMessage));
  }
  public function unfollow () {
    $this->autoRender = false;
    if ($this->CheckRequest('post')) {
        if ($this->CheckSession('User.token')) {
            $data = $this->request->getData();
            $session = $this->request->session();
            $baseToken = $session->read('User.token');
            $baseId = $session->read('User.id');
            $data['user_id'] = $this->idDecryption($data['user_id']);
            $data['id'] = $this->idDecryption($data['id']);
            if ($data['token'] === $baseToken && $baseId === $data['user_id']) {
                $data['deleted_date'] = date("Y-m-d H:i:s");
                $data['deleted'] = false;
                $record = $this->Followers->get($data['id']);
                unset($data['post_id']);
                $record = $this->Followers->patchEntity($record,$data);
                if ($this->Followers->save($record)) {
                    $this->promtMessage = array('status'=>'success', 'message'=>'unfollow successful');
                } else {
                    $errorList = [];
                    $errors = $record->errors();
                    foreach ($errors as $key => $value) {
                      foreach ($value as $key2 => $value2) {
                        array_push($errorList,array($key => $value2));
                      }
                    }
                    $this->promtMessage = array('status'=>'failed', 'message'=> $errorList);
                }
            } else {
                $this->promtMessage = array('status'=>'failed', 'message'=>'unauthorized');
            }
        }
    }
    return $this->response
    ->withHeader('token', 'My header')
    ->withType('application/json')
    ->withStringBody(json_encode($this->promtMessage));
  }
  public function searchPeople () {
    $this->autoRender = false;
    $userId = $this->request->query('id');
    $userId = intval($this->cleanNumber($this->idDecryption($userId)));
    $token = $this->cleanString($this->request->query('token'));
    $page = $this->cleanNumber($this->request->query('page'));
    $size = $this->cleanNumber($this->request->query('size'));
    $search = '%'.(empty($this->cleanString($this->request->query('search'))) ? 'blank' : $this->cleanString($this->request->query('search'))).'%';
    $peopleIds = [];
    if ($this->CheckRequest('get')) {
        if ($this->CheckSession('User.token')) {
            $session = $this->request->session();
            $baseToken = $session->read('User.token');
            $baseId = $session->read('User.id');
            if ($token === $baseToken && $baseId === $userId) {
                $Users = TableRegistry::getTableLocator()->get('Users');
                $offset = ($page - 1) * $size;
                $peopleName = $Users->find('all')
                  ->select(['id','first_name','last_name','image'])
                  ->where(['OR' => ['first_name LIKE' => $search,'middle_name LIKE' => $search,'last_name LIKE' => $search],'deleted'=>1,'activation_status'=>1,'id <>'=>$userId])
                  ->order(['created' => 'ASC'])
                  ->limit($size)
                  ->offset($offset)
                  ->formatResults(function($results) {
                      return $results->map(function($row) {
                        $row['created'] = date("M j Y g:i:s A", strtotime($row['created']));
                        return $row;
                      });
                  })
                  ->toArray();
                if (!empty($peopleName)) {
                    for ($i=0; $i < sizeof($peopleName) ; $i++) {
                      array_push($peopleIds,$peopleName[$i]['id']);
                      $myFollower = $this->Followers->find()->where(['user_id' => $peopleName[$i]['id'],'following_id' => $userId,'deleted' => 1])
                        ->order(['created' => 'ASC'])
                        ->limit($size)
                        ->offset($offset)
                        ->count();
                      $myFollowing = $this->Followers->find('all')
                        ->where(['user_id' => $userId ,'following_id' => $peopleName[$i]['id'],'deleted' => 1])
                        ->order(['created' => 'ASC'])
                        ->limit($size)
                        ->offset($offset)
                        ->toArray();
                        if ($myFollower === 1) {
                            $peopleName[$i]['myFollower'] = true;
                        }
                        if (sizeof($myFollowing) === 1) {
                            $peopleName[$i]['myFollowing'] = true;
                            $peopleName[$i]['myFollowingId'] = $myFollowing[0]['id'];
                        }
                    }
                    $total = $Users->find()
                      ->where(['OR' => ['first_name LIKE' => $search,'middle_name LIKE' => $search,'last_name LIKE' => $search],'deleted'=>1,'activation_status'=>1,'id <>'=>$userId])
                      ->count();
                    for ($i=0; $i < sizeof($peopleName); $i++) { 
                      $peopleName[$i]['myFollowingId'] = $this->idEncryption($peopleName[$i]['myFollowingId']);
                    }
                    $this->promtMessage = array('status'=>'success', 'people'=>$peopleName,'total'=>$total,'totalPages'=>(ceil($total/$size)));  
                } else {
                    $people = [];
                    $this->promtMessage = array('status'=>'success', 'people'=>$peopleName);
                }
            } else {
                $this->promtMessage = array('status'=>'failed', 'message'=>'unauthorized');
            }
        }
    }
    return $this->response
    ->withHeader('token', 'My header')
    ->withType('application/json')
    ->withStringBody(json_encode($this->promtMessage));
  }
}

<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Comments Controller
 *
 * @property \App\Model\Table\CommentsTable $Comments
 *
 * @method \App\Model\Entity\Comment[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class CommentsController extends AppController {
  public function viewComments () {
    $this->autoRender = false;
    $userId = $this->request->query('id');
    $userId = intval($this->cleanNumber($this->idDecryption($userId)));
    $postId = $this->cleanNumber($this->request->query('postId'));
    $token = $this->cleanString($this->request->query('token'));
    if ($this->CheckRequest('get')) {
      if ($this->CheckSession('User.token')) {
          $session = $this->request->session();
          $baseToken = $session->read('User.token');
          $baseId = $session->read('User.id');
          if ($token === $baseToken && $userId === $baseId) {
              $comments = $this->Comments->find('all')
              ->contain(['User'])
              ->select($this->Comments)
              ->select(['User.first_name','User.last_name','User.image'])
              ->where(['post_id' => $postId,'Comments.deleted' => 1])
              ->order(['Comments.created' => 'ASC'])
              ->formatResults(function($results) {
                return $results->map(function($row) {
                  $row['created'] = date("M j Y g:i:s A", strtotime($row['created']));
                  return $row;
                });
            });
              $this->promtMessage = array('status'=>'failed', 'message'=>'records not found');
              if (!empty($comments)) {
                  $this->promtMessage = array('status'=>'success','record'=>$comments);
              } 
          } else {
              $this->promtMessage = array('status'=>'failed', 'message'=>'unauthorized');
          }
      }
    }
    return $this->response
    ->withHeader('token', 'My header')
    ->withType('application/json')
    ->withStringBody(json_encode($this->promtMessage));
  }
  public function saveComment () {
    $this->autoRender = false;
    if ($this->CheckRequest('post')) {
        if ($this->CheckSession('User.token')) {
            $data = $this->request->getData();
            $session = $this->request->session();
            $baseToken = $session->read('User.token');
            $baseId = $session->read('User.id');
            $data['user_id'] = $this->idDecryption($data['user_id']);
            if ($data['token'] === $baseToken && $baseId === $data['user_id']) {
                $comment = $this->Comments->newEntity();
                $comment = $this->Comments->patchEntity($comment, $data);
                if ($this->Comments->save($comment)) {
                    $this->promtMessage = array('status'=>'success', 'message'=>'comment saved');
                } else {
                    $errorList = [];
                    $errors = $comment->errors();
                    foreach ($errors as $key => $value) {
                      foreach ($value as $key2 => $value2) {
                        array_push($errorList,array($key => $value2));
                      }
                    }
                    $this->promtMessage = array('status'=>'failed', 'message'=> $errorList);
                }
            } else {
                $this->promtMessage = array('status'=>'failed', 'message'=>'unauthorized');
            }
        }
    }
    return $this->response
    ->withHeader('token', 'My header')
    ->withType('application/json')
    ->withStringBody(json_encode($this->promtMessage));
  }
}

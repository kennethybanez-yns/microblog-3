<?php
/**
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link      https://cakephp.org CakePHP(tm) Project
 * @since     0.2.9
 * @license   https://opensource.org/licenses/mit-license.php MIT License
 */
namespace App\Controller;

use Cake\Controller\Controller;
use Cake\Event\Event;
use Cake\Mailer\Email;
use Cake\ORM\TableRegistry;
use Cake\Auth\DefaultPasswordHasher;
use Cake\I18n\Time;

/**
 * Application Controller
 *
 * Add your application-wide methods in the class below, your controllers
 * will inherit them.
 *
 * @link https://book.cakephp.org/3/en/controllers.html#the-app-controller
 */
class AppController extends Controller
{
    /**
     * Initialization hook method.
     *
     * Use this method to add common initialization code like loading components.
     *
     * e.g. `$this->loadComponent('Security');`
     *
     * @return void
     */
    private $requestMethod;
    public $promtMessage;
    private $session;
    // email sender variables
    public $link;
    public $name;
    public $token;
    public $email;
    // code for validation
    private $code;
    public $idEncryptor = "coronavirus2019";
    

    public function initialize() {
      parent::initialize();

      $this->loadComponent('RequestHandler', [
          'enableBeforeRedirect' => false,
      ]);
      $this->loadComponent('Flash');
      //$this->loadComponent('Csrf');

      /*
        * Enable the following component for recommended CakePHP security settings.
        * see https://book.cakephp.org/3/en/controllers/components/security.html
        */
      //$this->loadComponent('Security');
    }
    public function CheckRequest ($method) {
      $this->requestMethod = $method;
      if ($this->request->is($this->requestMethod)) {
          return true;
      } else {
          $this->promtMessage = array('status'=>'failed', 'message'=>'wrong request method');
          return false;
      }
    }
    public function CheckSession ($sessionValue) {
      $session = $this->request->session();
      if ($session->check($sessionValue)) {
          return true;
      } else {
          $this->promtMessage = array('status'=>'failed', 'message'=>'unauthorized');
          return false;
      }
    }
    public function capitalizeFirstLetter ($word) {
      $formattedWord = ucwords($word);
      return $formattedWord;
    }
    public function createCode () {
      $this->code = rand(1000, 999999); 
      return $this->code;
    }
    public function sendValidationLink ($token,$name,$email) {
      try {
          $this->link = 'http://dev9.ynsdev.pw/activate-account.php';
          $this->name = $name;
          $this->token = $token;
          $this->email = $email;
          $email = new Email();
          $email->profile('gmail');
          $email->from(['microblog.yns@gmail.com' => 'Microblog3'])
          ->to('kennethybanez.yns@gmail.com')
          ->template('default', 'default')
          ->emailFormat('html')
          ->setViewVars(['link' => $this->link,'name' => $this->name,'token' => $this->token ])
          ->subject('Email Verification')
          ->send();
      } catch (\Throwable $th) {
          $this->promtMessage = array('status'=>'emailProblem', 'message'=>'There is a problem sending verification code to email');
      }
    }
    public function checkPassword ($pw,$basePw) {
      $passwordHasher = new DefaultPasswordHasher();
      $pw = $passwordHasher->check($pw,$basePw);
      return $pw;
    }
    public function createToken ($username) {
      $passwordHasher = new DefaultPasswordHasher();
      $token = $passwordHasher->hash($username);
      return $token;
    }
    public function cleanstring($value) {
      // Removes leading and trailing spaces
      $data = trim($value);
      // Removes Unwanted Characters
      $data = filter_var($data, FILTER_SANITIZE_STRING);
      // Sanitizes HTML Characters
      $data = htmlspecialchars_decode($data, ENT_QUOTES);
      return $data;
    }
    public function cleanNumber($value) {
      // Removes leading and trailing spaces
      $data = trim($value);
      // Removes Unwanted Characters
      $data = filter_var($data, FILTER_SANITIZE_NUMBER_INT);
      // Sanitizes HTML Characters
      $data = htmlspecialchars_decode($data, ENT_QUOTES);
      return $data;
    }
    public function idEncryption($id) {
      $data = openssl_encrypt($id, "AES-128-ECB", $this->idEncryptor);
      //AyYEF91D0AisI0CHxk2+0w==
      return $data;
    }
    public function idDecryption($id) {
      $data = openssl_decrypt($id, "AES-128-ECB", $this->idEncryptor);
      //AyYEF91D0AisI0CHxk2+0w==
      $data = $this->cleanNumber($data);
      return intval($data);
    }
}

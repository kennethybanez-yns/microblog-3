<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Likes Controller
 *
 * @property \App\Model\Table\LikesTable $Likes
 *
 * @method \App\Model\Entity\Like[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class LikesController extends AppController {
  public function likePost () {
    $this->autoRender = false;
    if ($this->CheckRequest('post')) {
        if ($this->CheckSession('User.token')) {
            $data = $this->request->getData();
            $session = $this->request->session();
            $baseToken = $session->read('User.token');
            $baseId = $session->read('User.id');
            $data['user_id'] = $this->idDecryption($data['user_id']);
            if ($data['token'] === $baseToken && $baseId === $data['user_id']) {
                $duplicateCount = $this->Likes->find()->where(['post_id' => $data['post_id'],'user_id'=>$data['user_id'],'deleted'=>1])->count();
                if ($duplicateCount < 1) {
                  $like = $this->Likes->newEntity();
                  $like = $this->Likes->patchEntity($like, $data);
                  if ($this->Likes->save($like)) {
                      $this->promtMessage = array('status'=>'success','message'=>'You like this post');
                  } else {
                      $errorList = [];
                      $errors = $like->errors();
                      foreach ($errors as $key => $value) {
                        foreach ($value as $key2 => $value2) {
                          array_push($errorList,array($key => $value2));
                        }
                      }
                      $this->promtMessage = array('status'=>'failed', 'message'=> $errorList);
                  }
                } 
                else {
                    $query = $this->Likes->find('all')
                    ->where(['user_id' => $data['user_id'], 'post_id' => $data['post_id'], 'deleted' => 1]);
                    $record = $query->first(); 
                    $record['deleted'] = false;
                    $record['date_deleted'] = date("Y-m-d H:i:s");
                    if ($this->Likes->save($record)) {
                        $this->promtMessage = array('status'=>'unlike','message'=>'You unlike this post');
                    }
                }
            } else {
              $this->promtMessage = array('status'=>'failed', 'message'=>'unauthorized');
            }
        }
    }
    return $this->response
    ->withHeader('token', 'My header')
    ->withType('application/json')
    ->withStringBody(json_encode($this->promtMessage));
  }
}

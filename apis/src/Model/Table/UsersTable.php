<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use Cake\Event\Event;

/**
 * Users Model
 *
 * @property \App\Model\Table\CommentsTable&\Cake\ORM\Association\HasMany $Comments
 * @property \App\Model\Table\FollowersTable&\Cake\ORM\Association\HasMany $Followers
 * @property \App\Model\Table\LikesTable&\Cake\ORM\Association\HasMany $Likes
 * @property \App\Model\Table\PostsTable&\Cake\ORM\Association\HasMany $Posts
 *
 * @method \App\Model\Entity\User get($primaryKey, $options = [])
 * @method \App\Model\Entity\User newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\User[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\User|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\User saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\User patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\User[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\User findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class UsersTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config) {
      parent::initialize($config);

      $this->setTable('users');
      $this->setDisplayField('id');
      $this->setPrimaryKey('id');

      $this->addBehavior('Timestamp');

      // $this->hasMany('Comments', [
      //     'foreignKey' => 'user_id',
      // ]);
      // $this->hasMany('Followers', [
      //     'foreignKey' => 'user_id',
      // ]);
      // $this->hasMany('Likes', [
      //     'foreignKey' => 'user_id',
      // ]);
      // $this->hasMany('Posts', [
      //     'foreignKey' => 'user_id',
      // ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator) {
        $validator
            ->scalar('username')
            ->maxLength('username', 255)
            ->requirePresence('username', 'create')
            ->add('username', [
                'length' => [
                  'rule' => ['minLength', 8],
                  'message' => 'Username need to be at least 10 characters long',
                ]
            ])
            ->notEmptyString('username');

        $validator
            ->scalar('password')
            ->requirePresence('password', 'create')
            ->add('password', [
              'length' => [
                'rule' => ['minLength', 6],
                'message' => 'password need to be at least 6 characters long',
              ]
              ])
            ->notBlank('password', 'password should not be empty')
            ->notEmptyString('password','password should not be empty');

        $validator
            ->email('email')
            ->add('email', 'validFormat', [
              'rule' => 'email',
              'message' => 'E-mail must be valid'
            ])
            ->notEmptyString('email','email should not be empty');

        $validator
            ->scalar('first_name')
            ->notBlank('first_name', 'firstname should not be empty')
            ->maxLength('first_name', 255)
            ->notEmptyString('first_name','firstname should not be empty');

        $validator
            ->scalar('middle_name')
            ->notBlank('middle_name', 'middlename should not be empty')
            ->maxLength('middle_name', 255)
            ->notEmptyString('middle_name','middlename should not be empty');

        $validator
            ->scalar('last_name')
            ->notBlank('last_name', 'lastname should not be empty')
            ->maxLength('last_name', 255)
            ->notEmptyString('last_name','lastname should not be empty');

        $validator
            ->date('date_of_birth')
            ->add('date_of_birth', 'custom', [
                'rule' => function ($value, $context) {
                  if ($value >= date("Y-m-d")) {
                      return 'Invalid date of birthday';
                  }
                  return true;
                },
                'message' => 'Invalid date of birthday'
            ])
            ->notEmptyString('date_of_birth','birthday should not be empty');

        $validator
            ->scalar('image')
            ->maxLength('image', 255)
            ->allowEmptyFile('image');

        $validator
            ->scalar('code')
            ->maxLength('code', 255)
            ->allowEmptyString('code');

        $validator
            ->boolean('activation_status')
            ->notEmptyString('activation_status');

        $validator
            ->dateTime('deleted_date')
            ->allowEmptyDateTime('deleted_date');

        $validator
            ->boolean('deleted')
            ->notEmptyString('deleted');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules) {
      $rules->add($rules->isUnique(['username']));
      $rules->add($rules->isUnique(['email']));
      return $rules;
    }
    public function clean_string($value) {
      // Removes leading and trailing spaces
      $data = trim($value);
      // Removes Unwanted Characters
      $data = filter_var($data, FILTER_SANITIZE_STRING);
      // Sanitizes HTML Characters
      $data = htmlspecialchars_decode($data, ENT_QUOTES);
      return $data;
    }
    public function beforeSave(Event $event) {
      $entity = $event->getData('entity');
      if (!empty($entity->first_name)) {
          $entity->first_name = $this->clean_string($entity->first_name);
      }
      if (!empty($entity->middle_name)) {
          $entity->middle_name = $this->clean_string($entity->middle_name);
      }
      if (!empty($entity->last_name)) {
          $entity->last_name = $this->clean_string($entity->last_name);
      }
      if (!empty($entity->username)) {
          $entity->username = $this->clean_string($entity->username);
      }
    }
}

<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\ORM\TableRegistry;

/**
 * Posts Controller
 *
 * @property \App\Model\Table\PostsTable $Posts
 *
 * @method \App\Model\Entity\Post[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class PostsController extends AppController {
  public function addPost () {
    $this->autoRender = false;
    if ($this->CheckRequest('post')) {
        if ($this->CheckSession('User.token')) {
            $data = $this->request->getData();
            $session = $this->request->session();
            $baseToken = $session->read('User.token');
            $baseId = $session->read('User.id');
            $postImages = [];
            $data['user_id'] = $this->idDecryption($data['user_id']);
            if ($data['token'] === $baseToken && $baseId === $data['user_id']) {
                $go = true;
                if (!empty($_FILES['file'])) {
                    $totalPosts = $this->Posts->find()->where(['user_id' => $data['user_id']])->count() + 1;
                    $no_files = count($_FILES["file"]['name']);
                    for ($i=0; $i < $no_files; $i++) {
                      if ($_FILES['file']['size'][$i] > 2000000) {
                          $this->promtMessage = array('status'=>'failed', 'message'=>('Image number '.($i+1).' should not exceed size of 2mb'));
                          $go = false;
                      } else {
                          $img = $_FILES['file']['name'][$i];
                          $tmp = $_FILES['file']['tmp_name'][$i];
                          $path = '../../pic-posts/';
                          $extension = pathinfo($img, PATHINFO_EXTENSION);
                          $path = $path.strtolower($totalPosts.$data['user_id'].$i."-postpic.".$extension);
                          $imgNewName = strtolower($totalPosts.$data['user_id'].$i."-postpic.".$extension);
                          array_push($postImages,$imgNewName);
                          if (!move_uploaded_file($tmp,$path)) {
                              $this->promtMessage = array('status'=>'failed', 'message'=>"Image not uploaded to server and database");
                          }  
                      }
                    }
                    $data['images'] = json_encode($postImages);
                }
                if ($go) {
                    $post = $this->Posts->newEntity();
                    $post = $this->Posts->patchEntity($post, $data);
                    if ($this->Posts->save($post)) {
                        $this->promtMessage = array('status'=>'success', 'message'=>'Your blog was uploaded');
                    } else {
                        $errorList = [];
                        $errors = $post->errors();
                        foreach ($errors as $key => $value) {
                          foreach ($value as $key2 => $value2) {
                            array_push($errorList,array($key => $value2));
                          }
                        }
                        $this->promtMessage = array('status'=>'failed', 'message'=> $errorList);
                    }
                } 
            } else {
                $this->promtMessage = array('status'=>'failed', 'message'=>'unauthorized');
            }
        }
    }
    return $this->response
    ->withHeader('token', 'My header')
    ->withType('application/json')
    ->withStringBody(json_encode($this->promtMessage));
  }
  public function viewAllBlogs () {
    $this->autoRender = false;
    $userId = $this->request->query('id');
    $userId = intval($this->cleanNumber($this->idDecryption($userId)));
    $token = $this->cleanString($this->request->query('token'));
    $page = $this->cleanNumber($this->request->query('page'));
    $size = $this->cleanNumber($this->request->query('size'));
    if ($this->CheckRequest('get')) {
        if ($this->CheckSession('User.token')) {
            $session = $this->request->session();
            $baseToken = $session->read('User.token');
            $baseId = $session->read('User.id');
            if ($token === $baseToken && $userId === $baseId) {
                $followingModel = TableRegistry::getTableLocator()->get('Followers');
                $offset = ($page - 1) * $size;
                $followingIds = [];
                $followings =  $followingModel->find('all',array(
                  'conditions'=>array('Follower.user_id'=>$userId,'Follower.deleted'=>1),
                  'order'=> array('Follower.created ASC'),
                ));
                $followings = $followingModel->find('all')
                  ->where(['deleted'=>1,'user_id'=>$userId])
                  ->order(['created' => 'ASC'])
                  ->toArray();
                for ($followingCount=0; $followingCount < sizeof($followings); $followingCount++) { 
                  array_push($followingIds,$followings[$followingCount]['following_id']);
                }
                array_push($followingIds,$userId);
                $total = $this->Posts->find()->where(['user_id IN' => $followingIds,'deleted' => 1])->count();
                $blogs = $this->Posts->find('all')
                ->contain(['User','Retweet','RetweetOwner','Likes','Comments','Shares'])
                ->select($this->Posts)
                ->select(['RetweetOwner.id','RetweetOwner.first_name','RetweetOwner.last_name','RetweetOwner.image','Retweet.id','Retweet.user_id','Retweet.post','Retweet.images','Retweet.image_captions','Retweet.modified','Retweet.deleted','User.id','User.first_name','User.last_name','User.image'])
                ->where(['Posts.user_id IN' => $followingIds,'Posts.deleted' => 1])
                ->limit($size)
                ->order(['Posts.created' => 'DESC'])
                ->formatResults(function($results) {
                    return $results->map(function($row) {
                      $row['modified'] = date("M j Y g:i:s A", strtotime($row['modified']));
                      if (isset($row['images'])) {
                          $row['images'] = json_decode($row['images']);
                      }
                      if (isset($row['image_captions'])) {
                        $row['image_captions'] = json_decode($row['image_captions']);
                      }
                      if (isset($row['retweet']['images'])) {
                        $row['retweet']['images'] = json_decode($row['retweet']['images']);
                      }
                      if (isset($row['retweet']['image_captions'])) {
                        $row['retweet']['image_captions'] = json_decode($row['retweet']['image_captions']);
                      }
                      if (isset($row['retweet']['modified'])) {
                        $row['retweet']['modified'] = date("M j Y g:i:s A", strtotime($row['retweet']['modified']));
                      }
                      return $row;
                    });
                })
                ->offset($offset);
                if (!empty($blogs)) {
                    $this->promtMessage = array('status'=>'success','total'=>$total,'totalPages'=>(ceil($total/$size)),'record'=>$blogs);
                }
            } else {
                $this->promtMessage = array('status'=>'failed', 'message'=>'unauthorized');
            }
        }
    }
    return $this->response
    ->withHeader('token', 'My header')
    ->withType('application/json')
    ->withStringBody(json_encode($this->promtMessage));
  }
  public function viewMyBlogs () {
    $this->autoRender = false;
    $userId = $this->request->query('id');
    $userId = intval($this->cleanNumber($this->idDecryption($userId)));
    $token = $this->cleanString($this->request->query('token'));
    $page = $this->cleanNumber($this->request->query('page'));
    $size = $this->cleanNumber($this->request->query('size'));
    if ($this->CheckRequest('get')) {
        if ($this->CheckSession('User.token')) {
            $session = $this->request->session();
            $baseToken = $session->read('User.token');
            $baseId = $session->read('User.id');
            if ($token === $baseToken && $userId === $baseId) {
                $offset = ($page - 1) * $size;
                $total = $this->Posts->find()->where(['user_id' => $userId,'deleted' => 1])->count();
                $blogs = $this->Posts->find('all')
                ->contain(['User','Retweet','RetweetOwner','Likes','Comments','Shares'])
                ->select($this->Posts)
                
                ->select(['RetweetOwner.id','RetweetOwner.first_name','RetweetOwner.last_name','RetweetOwner.image','Retweet.id','Retweet.user_id','Retweet.post','Retweet.images','Retweet.image_captions','Retweet.modified','Retweet.deleted','User.id','User.first_name','User.last_name','User.image'])
                ->where(['Posts.user_id' => $userId,'Posts.deleted' => 1])
                ->limit($size)
                ->order(['Posts.created' => 'DESC'])
                ->formatResults(function($results) {
                    return $results->map(function($row) {
                      $row['modified'] = date("M j Y g:i:s A", strtotime($row['modified']));
                      if (isset($row['images'])) {
                          $row['images'] = json_decode($row['images']);
                      }
                      if (isset($row['image_captions'])) {
                        $row['image_captions'] = json_decode($row['image_captions']);
                      }
                      if (isset($row['retweet']['images'])) {
                        $row['retweet']['images'] = json_decode($row['retweet']['images']);
                      }
                      if (isset($row['retweet']['image_captions'])) {
                        $row['retweet']['image_captions'] = json_decode($row['retweet']['image_captions']);
                      }
                      if (isset($row['retweet']['modified'])) {
                        $row['retweet']['modified'] = date("M j Y g:i:s A", strtotime($row['retweet']['modified']));
                      }
                      return $row;
                    });
                })
                ->offset($offset);
                if (!empty($blogs)) {
                    $this->promtMessage = array('status'=>'success','total'=>$total,'totalPages'=>(ceil($total/$size)),'record'=>$blogs);
                }
            } else {
                $this->promtMessage = array('status'=>'failed', 'message'=>'unauthorized');
            }
        }
    }
    return $this->response
    ->withHeader('token', 'My header')
    ->withType('application/json')
    ->withStringBody(json_encode($this->promtMessage));
  }
  public function deletePost () {
    $this->autoRender = false;
    if ($this->CheckRequest('post')) {
        if ($this->CheckSession('User.token')) {
            $data = $this->request->getData();
            $session = $this->request->session();
            $baseToken = $session->read('User.token');
            $baseId = $session->read('User.id');
            $data['user_id'] = $this->idDecryption($data['user_id']);
            if ($data['token'] === $baseToken && $baseId === $data['user_id']) {
                $checkOwner = $this->Posts->find()->where(['id' => $data['post_id'],'user_id'=>$data['user_id']])->count();
                if ($checkOwner === 1) {
                    $data['deleted_date'] = date("Y-m-d H:i:s");
                    $data['deleted'] = false;
                    $record = $this->Posts->get($data['post_id']);
                    unset($data['post_id']);
                    $record = $this->Posts->patchEntity($record,$data);
                    if ($this->Posts->save($record)) {
                        $this->promtMessage = array('status'=>'success', 'message'=>'Your post was deleted');
                    } else {
                        $errorList = [];
                        $errors = $record->errors();
                        foreach ($errors as $key => $value) {
                          foreach ($value as $key2 => $value2) {
                            array_push($errorList,array($key => $value2));
                          }
                        }
                        $this->promtMessage = array('status'=>'failed', 'message'=> $errorList);
                    }
                } else {
                    $this->promtMessage = array('status'=>'failed', 'message'=>'unauthorized to delete this');
                }
            } else {
                $this->promtMessage = array('status'=>'failed', 'message'=>'unauthorized');
            }
        }
    }
    return $this->response
    ->withHeader('token', 'My header')
    ->withType('application/json')
    ->withStringBody(json_encode($this->promtMessage));
  }
  public function editPost () {
    $this->autoRender = false;
    if ($this->CheckRequest('post')) {
        if ($this->CheckSession('User.token')) {
            $data = $this->request->getData();
            $session = $this->request->session();
            $baseToken = $session->read('User.token');
            $baseId = $session->read('User.id');
            $data['existing_pics'] = json_decode($data['existing_pics'],true);
            $data['user_id'] = $this->idDecryption($data['user_id']);
            if ($data['token'] === $baseToken && $baseId === $data['user_id']) {
                $checkOwner = $this->Posts->find()->where(['id' => $data['post_id'],'user_id'=>$data['user_id']])->count();
                if ($checkOwner === 1) {
                    $go = true;
                    if (!empty($_FILES['file'])) {
                        $totalPosts = $this->Posts->find()->where(['user_id'=>$data['user_id']])->count(); + 1;
                        $no_files = count($_FILES["file"]['name']);
                        for ($i=0; $i < $no_files; $i++) {
                          if ($_FILES['file']['size'][$i] > 2000000) {
                              $this->promtMessage = array('status'=>'failed', 'message'=>('Some of your images exceeds the size of 2mb, select another image with lower size'));
                              $go = false;
                          } else {
                              $img = $_FILES['file']['name'][$i];
                              $tmp = $_FILES['file']['tmp_name'][$i];
                              $path = '../../pic-posts/';
                              $extension = pathinfo($img, PATHINFO_EXTENSION);
                              $path = $path.strtolower($totalPosts.$data['user_id'].'-'.date("Y-m-d-H-i-s").$i."-postpic.".$extension);
                              $imgNewName = strtolower($totalPosts.$data['user_id'].'-'.date("Y-m-d-H-i-s").$i."-postpic.".$extension);
                              array_push($data['existing_pics'],$imgNewName);
                              if (!move_uploaded_file($tmp,$path)) {
                                  $this->promtMessage = array('status'=>'failed', 'message'=>"Image not uploaded to server and database");
                              }
                          }
                        }
                        $data['images'] = json_encode($data['existing_pics']);
                    } else {
                        $data['images'] = json_encode($data['existing_pics']);
                    }
                    $record = $this->Posts->get($data['post_id']);
                    unset($data['post_id']);
                    $record = $this->Posts->patchEntity($record,$data);
                    if ($go) {
                        if ($this->Posts->save($record)) {
                            $this->promtMessage = array('status'=>'success', 'message'=>'Your blog was edited');
                        } else {
                            $errorList = [];
                            $errors = $record->errors();
                            foreach ($errors as $key => $value) {
                              foreach ($value as $key2 => $value2) {
                                array_push($errorList,array($key => $value2));
                              }
                            }
                            $this->promtMessage = array('status'=>'failed', 'message'=> $errorList);
                        }
                    }
                } else {
                    $this->promtMessage = array('status'=>'failed', 'message'=>'unauthorized to edit this');
                }
            } else {
                $this->promtMessage = array('status'=>'failed', 'message'=>'unauthorized');
            }
        }
    }
    return $this->response
    ->withHeader('token', 'My header')
    ->withType('application/json')
    ->withStringBody(json_encode($this->promtMessage));
  }
  public function sharePost () {
    $this->autoRender = false;
    if ($this->CheckRequest('post')) {
        if ($this->CheckSession('User.token')) {
            $data = $this->request->getData();
            $session = $this->request->session();
            $baseToken = $session->read('User.token');
            $baseId = $session->read('User.id');
            $data['user_id'] = $this->idDecryption($data['user_id']);
            if ($data['token'] === $baseToken && $baseId === $data['user_id']) {
                $rule = (empty($data['post']) ? false : true);
                $post = $this->Posts->newEntity();
                $post = $this->Posts->patchEntity($post, $data, [
                  'validate' => $rule]);
                if ($this->Posts->save($post)) {
                    $this->promtMessage = array('status'=>'success', 'message'=>'You shared this post');
                } else {
                    $errorList = [];
                    $errors = $post->errors();
                    foreach ($errors as $key => $value) {
                      foreach ($value as $key2 => $value2) {
                        array_push($errorList,array($key => $value2));
                      }
                    }
                    $this->promtMessage = array('status'=>'failed', 'message'=> $errorList);
                }
            } else {
                $this->promtMessage = array('status'=>'failed', 'message'=>'unauthorized');
            }
        }
    }
    return $this->response
    ->withHeader('token', 'My header')
    ->withType('application/json')
    ->withStringBody(json_encode($this->promtMessage));
  }
  public function searchAllBlogs () {
    $this->autoRender = false;
    $userId = $this->request->query('id');
    $userId = intval($this->cleanNumber($this->idDecryption($userId)));
    $token = $this->cleanString($this->request->query('token'));
    $page = $this->cleanNumber($this->request->query('page'));
    $size = $this->cleanNumber($this->request->query('size'));
    $search = '%'.(empty($this->cleanString($this->request->query('search'))) ? 'blank' : $this->cleanString($this->request->query('search'))).'%';
    if ($this->CheckRequest('get')) {
        if ($this->CheckSession('User.token')) {
            $session = $this->request->session();
            $baseToken = $session->read('User.token');
            $baseId = $session->read('User.id');
            if ($token === $baseToken && $userId === $baseId) {
                $followingModel = TableRegistry::getTableLocator()->get('Followers');
                $offset = ($page - 1) * $size;
                $followingIds = [];
                $followings =  $followingModel->find('all',array(
                  'conditions'=>array('Follower.user_id'=>$userId,'Follower.deleted'=>1),
                  'order'=> array('Follower.created ASC'),
                ));
                $followings = $followingModel->find('all')
                ->where(['deleted'=>1,'user_id'=>$userId])
                ->order(['created' => 'ASC'])
                ->toArray();
                for ($followingCount=0; $followingCount < sizeof($followings); $followingCount++) { 
                  array_push($followingIds,$followings[$followingCount]['following_id']);
                }
                array_push($followingIds,$userId);
                $total = $this->Posts->find()->where(['user_id IN' => $followingIds,'Posts.post LIKE '=>$search,'deleted' => 1])->count();
                $blogs = $this->Posts->find('all')
                ->contain(['User','Retweet','RetweetOwner','Likes','Comments','Shares'])
                ->select($this->Posts)
                ->select(['RetweetOwner.id','RetweetOwner.first_name','RetweetOwner.last_name','RetweetOwner.image','Retweet.id','Retweet.user_id','Retweet.post','Retweet.images','Retweet.image_captions','Retweet.modified','Retweet.deleted','User.id','User.first_name','User.last_name','User.image'])
                ->where(['Posts.user_id IN' => $followingIds,'Posts.post LIKE '=>$search,'Posts.deleted' => 1])
                ->limit($size)
                ->order(['Posts.created' => 'DESC'])
                ->formatResults(function($results) {
                    return $results->map(function($row) {
                      $row['modified'] = date("M j Y g:i:s A", strtotime($row['modified']));
                      if (isset($row['images'])) {
                          $row['images'] = json_decode($row['images']);
                      }
                      if (isset($row['image_captions'])) {
                        $row['image_captions'] = json_decode($row['image_captions']);
                      }
                      if (isset($row['retweet']['images'])) {
                        $row['retweet']['images'] = json_decode($row['retweet']['images']);
                      }
                      if (isset($row['retweet']['image_captions'])) {
                        $row['retweet']['image_captions'] = json_decode($row['retweet']['image_captions']);
                      }
                      if (isset($row['retweet']['modified'])) {
                        $row['retweet']['modified'] = date("M j Y g:i:s A", strtotime($row['retweet']['modified']));
                      }
                      return $row;
                    });
                })
                ->offset($offset);
                if (!empty($blogs)) {
                    $this->promtMessage = array('status'=>'success','total'=>$total,'totalPages'=>(ceil($total/$size)),'record'=>$blogs);
                }
            } else {
                $this->promtMessage = array('status'=>'failed', 'message'=>'unauthorized');
            }
        }
    }
    return $this->response
    ->withHeader('token', 'My header')
    ->withType('application/json')
    ->withStringBody(json_encode($this->promtMessage));
  }
}

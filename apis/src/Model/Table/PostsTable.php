<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use Cake\Event\Event;
/**
 * Posts Model
 *
 * @property \App\Model\Table\UsersTable&\Cake\ORM\Association\BelongsTo $Users
 * @property \App\Model\Table\PostsTable&\Cake\ORM\Association\BelongsTo $Posts
 * @property \App\Model\Table\CommentsTable&\Cake\ORM\Association\HasMany $Comments
 * @property \App\Model\Table\LikesTable&\Cake\ORM\Association\HasMany $Likes
 * @property \App\Model\Table\PostsTable&\Cake\ORM\Association\HasMany $Posts
 *
 * @method \App\Model\Entity\Post get($primaryKey, $options = [])
 * @method \App\Model\Entity\Post newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Post[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Post|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Post saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Post patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Post[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Post findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class PostsTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
      parent::initialize($config);

      $this->setTable('posts');
      $this->setDisplayField('id');
      $this->setPrimaryKey('id');

      $this->addBehavior('Timestamp');

      $this->belongsTo('User', [
        'className' => 'Users',
        'foreignKey' => 'user_id',
      ]);
      $this->belongsTo('Retweet', [
        'className' => 'Posts',
        'foreignKey' => 'post_id',
        'conditions' => ["Retweet.deleted = 1"]
      ]);
      $this->belongsTo('RetweetOwner', [
        'className' => 'Users',
        'foreignKey' => false,
        'conditions' => ["RetweetOwner.id = Retweet.user_id"]
      ]);
      $this->hasMany('Comments', [
          'className' => 'Comments',
          'foreignKey' => 'post_id',
          'conditions' => ["Comments.deleted = 1"]
      ]);
      $this->hasMany('Likes', [
          'className' => 'Likes',
          'foreignKey' => 'post_id',
          'conditions' => ["Likes.deleted = 1"]
      ]);
      $this->hasMany('Shares', [
        'className' => 'Posts',
          'foreignKey' => 'post_id',
          'conditions' => ["Shares.deleted = 1"]
      ]);

      // $this->belongsTo('Users', [
      //     'foreignKey' => 'user_id',
      //     'joinType' => 'INNER',
      // ]);
      // $this->belongsTo('Posts', [
      //     'foreignKey' => 'post_id',
      // ]);
      // $this->hasMany('Comments', [
      //     'foreignKey' => 'post_id',
      // ]);
      // $this->hasMany('Likes', [
      //     'foreignKey' => 'post_id',
      // ]);
      // $this->hasMany('Posts', [
      //     'foreignKey' => 'post_id',
      // ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator) {
      $validator
          ->scalar('post')
          ->requirePresence('post', 'create')
          ->add('post', [
            'length' => [
              'rule' => ['maxLength', 150],
              'message' => 'post must not be more than 150 charcters',
            ]
          ])
        ->notEmptyString('post','post should not be empty');

      $validator
          ->scalar('images')
          ->maxLength('images', 255)
          ->allowEmptyFile('images');

      $validator
          ->scalar('image_captions')
          ->allowEmptyFile('image_captions');

      $validator
          ->dateTime('deleted_date')
          ->allowEmptyDateTime('deleted_date');

      $validator
          ->boolean('deleted')
          ->notEmptyString('deleted');

      return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    // public function buildRules(RulesChecker $rules) {
    //     // $rules->add($rules->existsIn(['user_id'], 'Users'));
    //     // $rules->add($rules->existsIn(['post_id'], 'Posts'));

    //     // return $rules;
    // }
    public function clean_string($value) {
      // Removes leading and trailing spaces
      $data = trim($value);
      // Removes Unwanted Characters
      $data = filter_var($data, FILTER_SANITIZE_STRING);
      // Sanitizes HTML Characters
      $data = htmlspecialchars_decode($data, ENT_QUOTES);
      return $data;
    }
    public function beforeSave(Event $event) {
      $entity = $event->getData('entity');
      if (!empty($entity->post)) {
          $entity->post = $this->clean_string($entity->post);
      }
    }
    public function afterSave(Event $event) {
      $entity = $event->getData('entity');
      if (!empty($entity->image_captions)) {
          $entity->image_captions = null;
      }
    }
    
}
